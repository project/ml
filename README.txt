TRANSLATION TEMPLATES FOR DRUPAL 6.0
================================================================================

Upload the files into their respective folders
If a translations folder does not exist, you would need to create a folder called translations



The translation files are for the malayalam translation of Drupal 6.0.

Translation done by Sunil Kumar

Release notes cane be found on 
http://malayalam.indiandrupal.com/node/41

Updating Drupal 5 translations:
--------------------------------

Start at http://drupal.org/node/202631


File placement and naming is very important. Translation files are
automtically imported from module and theme folders, and used from install
profile folders on installation. The following placement is used
by the packager based on the template file set in this directory:

 - general.ml.po, includes.po, misc.po and modules-system.po
   translations to modules/system/translations
   
 - modules-*.po translations to their corresponding modules/*/translations
   directory
   
 - themes-*.po translations to their corresponding themes/*/translations
   directory
   
 - installer.po to profiles/default/translations
   as ml.po 
   
   
